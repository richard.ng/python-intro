from itertools import permutations
# cat /usr/share/dict/words > words.txt

startword = input("Enter a word: ")

w = open("words.txt","r").readlines()
for i in range(len(w)):
	w[i]=w[i].strip()

perms = [''.join(p) for p in permutations(startword)]

list=[]
for i in range(len(perms)):
	if perms[i] in w:
		list.append(perms[i])

set = set(list)

for word in set:
    if word == startword: continue
    print(word)
