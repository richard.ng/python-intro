# Python intro

This will be a quick introduction to basic python programming. The simple apps created in this repo will cover the following aspects:

- [ ] How to set up the environment to write code in that language.
- [ ] How to assign values to variables.
- [ ] How to select between two paths (Usually an ‘if’ statement).
- [ ] How to iterate over a block of code.
- [ ] How to create and manipulate basic data structures.
- [ ] How to create functions/methods or subroutines.
- [ ] How to perform basic mathematical calculations (+ — * /)
- [ ] How to communicate with the file system.
- [ ] How to print to the console/prompt.
- [ ] How to write tests

## App #1: The ‘Hello World’ app

Acceptance criteria:
- Must print the phrase ‘Hello World!’ to a console or prompt.

Aspects covered:
- [x] How to set up the environment to write code in that language.
- [ ] How to assign values to variables.
- [ ] How to select between two paths (Usually an ‘if’ statement).
- [ ] How to iterate over a block of code.
- [ ] How to create and manipulate basic data structures.
- [ ] How to create functions/methods or subroutines.
- [ ] How to perform basic mathematical calculations (+ — * /)
- [ ] How to communicate with the file system.
- [x] How to print to the console/prompt.
- [ ] How to write tests

## App #2: The ‘Anagram’ app

Acceptance criteria:
- Take a string through the console input
- Take in a file with a dictionary of useable words.
- Output a list of words to the console that are an anagram of the inputted string
- Be Tested!

Aspects covered:
- [x] How to set up the environment to write code in that language.
- [x] How to assign values to variables.
- [x] How to select between two paths (Usually an ‘if’ statement).
- [ ] How to iterate over a block of code.
- [x] How to create and manipulate basic data structures.
- [x] How to create functions/methods or subroutines.
- [ ] How to perform basic mathematical calculations (+ — * /)
- [x] How to communicate with the file system.
- [x] How to print to the console/prompt.
- [x] How to add tests

## App #3: The pizza app

Acceptance criteria:
- Prints a list of pizzas on the menu to the console.
- User can select a pizza and add to basket.
- Basket displays a subtotal.
- Basket displays the tax amount (% of your choice).

Aspects covered:
- [x] How to set up the environment to write code in that language.
- [x] How to assign values to variables.
- [x] How to select between two paths (Usually an ‘if’ statement).
- [x] How to iterate over a block of code.
- [x] How to create and manipulate basic data structures.
- [x] How to create functions/methods or subroutines.
- [x] How to perform basic mathematical calculations (+ — * /)
- [x] How to communicate with the file system.
- [x] How to print to the console/prompt.
- [x] How to add tests

Source:
https://medium.com/@samuel.fare/want-to-learn-any-programming-language-write-these-3-simple-apps-5af8cd119921
