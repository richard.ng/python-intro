import json

print("Basket ------")
print("Total cost: 0.00")
print("------ ------")

menu = {
    "pepperoni": 23.99,
    "mozzerella": 24.99,
    "special": 25.99
}

print(json.dumps(menu,indent=2))

pick = input("Select a pizza: ")

if pick in menu:
    print(menu[pick])